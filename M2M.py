import json
import requests
import os
import sys
import geopandas as gpd
from werkzeug.utils import secure_filename
import datetime
from flask import Flask, render_template, request, jsonify
import concurrent.futures
from tqdm import tqdm

# Initializing Flask App.
app = Flask(__name__)

current_path = os.getcwd()


def fetch_url(url):
    response = requests.get(url)
    return response.text

# Defining landsat variables.
LANDSAT_8_9 = "Landsat 8-9 OLI/TIRS C2 L2"
LANDSAT_4_5 = "Landsat 4-5 TM C2 L2"

datasetname = ""
# Mapping over Datasetnames...
datasetname_mapping = {
        LANDSAT_8_9: "landsat_ot_c2_l2",
        LANDSAT_4_5: "landsat_tm_c2_l2",
        "ASTER Level 1T V3": "aster_l1t",
        "emodis_global_lst_v6": "emodis_global_lst_v6",
        "viirs_vnp13c2": "viirs_vnp13c2",
        "viirs_vnp13c1": "viirs_vnp13c1",
        "viirs_vnp13a3": "viirs_vnp13a3",
        "viirs_vnp13a2": "viirs_vnp13a2",
        "viirs_vnp21": "viirs_vnp21",
        "viirs_vnp13a1": "viirs_vnp13a1",
        "modis_mod13q1_v61": "modis_mod13q1_v61",
        "modis_mod11a2_v61": "modis_mod11a2_v61"
    }


sats = ["ASTER Level 1T V3","viirs_vnp13c2","emodis_global_lst_v6","viirs_vnp13c1","viirs_vnp13a3","viirs_vnp13a2","viirs_vnp21","viirs_vnp13a1","modis_mod13q1_v61","modis_mod11a2_v61"]

# Send http requests.
def send_request(url, data, newApiKey=None):
    json_data = json.dumps(data)

    if newApiKey == None:
        response = requests.post(url, json_data)
    else:
        headers = {'X-Auth-Token': newApiKey}
        response = requests.post(url, json_data, headers=headers)

    try:
        httpStatusCode = response.status_code
        # # if response == None:
        # print("No output from service")
        #     sys.exit()
        output = json.loads(response.text)
        if output['errorCode'] != None:
            print(output['errorCode'], "- ", output['errorMessage'])
            sys.exit()
        if httpStatusCode == 404:
            print("404 Not Found")
            sys.exit()
        elif httpStatusCode == 401:
            print("401 Unauthorized")
            sys.exit()
        elif httpStatusCode == 400:
            print("Error Code", httpStatusCode)
            sys.exit()
    except Exception as e:
        response.close()
        print(e)
        sys.exit()
    response.close()

    return output['data']


@app.route('/')
def map_route():
    return render_template('map.html')


@app.route('/uploader', methods = ['POST'])
def upload_fil():


    n_co = []
    if request.method == 'POST':
        shapefiles = request.files.getlist('shapefiles')
        sat = request.form.get('sat')
        dateF = request.form.get('dateF')
        dateT = request.form.get('dateT')
        clMa = request.form.get('cloudMa')
        clMi = request.form.get('cloudMi')
        # print(shapefiles)
        print(sat)
        print(dateF)
        print(dateT)
        print(clMa)
        print(clMi)

        # print(shapefiles[2])
        prefix = shapefiles[0].filename[:-4]
        print(prefix)
        accord = True

        for file in shapefiles:
            # print("here1!!!!")
            if file.filename.split('.')[0] != prefix:
                accord = False
                break

        if accord == True:

            for file in shapefiles:
                print(file.filename.split('.')[-1])
                if file.filename.split('.')[-1] == "shp":
                    global dotshp
                    dotshp = secure_filename(file.filename)
                    file.save(os.path.join(current_path, file.filename))
                elif file.filename.split('.')[-1] == "shx":
                    dotshx = secure_filename(file.filename)
                    file.save(os.path.join(current_path, file.filename))
                elif file.filename.split('.')[-1] == "dbf":
                    dotdbf = secure_filename(file.filename)
                    file.save(os.path.join(current_path, file.filename))
                else:
                    print("The allowed extensions are: ['.shp', '.shx', '.dbf']")
                    accord = False
                    break

            path  =  current_path + '\\' + dotshp
            data = gpd.read_file(path)

                # Extract the coordinates
            coordinates = []

            for geom in data.geometry:
                        #If the geometry is for a polygon.
                        if geom.geom_type == 'Polygon':
                            coordinates.append(geom.exterior.coords[:])
                        #Geometry of a Multipolygon.
                        elif geom.geom_type == 'MultiPolygon':
                            for polygon in geom.geoms:
                                coordinates.append(polygon.exterior.coords[:])
                        else:
                            # Handle other geometry types if needed
                            pass
            for coords in coordinates:
                # print(coords)
                for c in coords:
                    l  = list(c)
                    n_co.append([l[1], l[0]])
                break
                # break
            # print(n_co)

        else:
            print("Check if all the files have the same prefix!")

    print("\nRunning Scripts...\n")

    serviceUrl = "https://m2m.cr.usgs.gov/api/api/json/development/"

    # login
    payload = {'username': 'marouan.essalhi@uit.ac.ma',
               'password': "Morroco@1212"}

    newApiKey = send_request(serviceUrl + "login", payload)
    temporalFilter = {'start': dateF, 'end': dateT}
    CloudCoverFilter = {"min": int(clMi), "max": int(
        clMa), "includeUnknown": True}

    print("API Key: " + newApiKey + "\n")


    if sat in datasetname_mapping:
        datasetname = datasetname_mapping[sat]



    payload = {
        "datasetName": datasetname,
        # "temporalFilter" : temporalFilter,
        # "cloudCoverFilter": CloudCoverFilter,
        "maxResults": "50",
        "metadataType": "summary",
        "sceneFilter": {
            "acquisitionFilter": temporalFilter,
            "cloudCoverFilter": CloudCoverFilter,
            "spatialFilter": {
                        "geoJson": {
                            "type": "MultiPolygon",
                            "coordinates": [
                                n_co
                            ]
                        },
                "filterType": "geojson"
            }

        }
    }

    print("Searching datasets...\n")
    scenes = send_request(serviceUrl + "scene-search", payload, newApiKey)



    print(scenes)
    if scenes['recordsReturned'] > 0:
        # Aggregate a list of scene ids
        sceneIds = []
        for result in scenes['results']:
            # Add this scene to the list I would like to download
            sceneIds.append(result['entityId'])
            # print(result['entityId'])
        # Find the download options for these scenes
        # NOTE :: Remember the scene list cannot exceed 50,000 items!
        payload = {'datasetName': datasetname.lower(), 'entityIds': sceneIds}

        downloadOptions = send_request(
            serviceUrl + "download-options", payload, newApiKey)
        # print(downloadOptions)

        # Aggregate a list of available products
        downloads = []
        # print(downloadOptions)
        # try:
        for product in downloadOptions:
                # Make sure the product is available for this scene
                if product['available'] == True:
                    downloads.append({'entityId': product['entityId'],
                                    'productId': product['id']})
                    print("done!!!")
                else:
                    print("No products are available")
        # except:
        #     print("Failed to find products")
            #  

        print("we are here!!!!!!!")
        if downloads:

            label = datetime.datetime.now().strftime(
                "%Y%m%d_%H%M%S")  # Customized label using date time
            payload = {'downloads': downloads,
                       'label': label}
            # Call the download to get the direct download urls
            requestResults = send_request(
                serviceUrl + "download-request", payload, newApiKey)

            # PreparingDownloads has a valid link that can be used but data may not be immediately available
            # Call the download-retrieve method to get download that is available for immediate download
            if requestResults['preparingDownloads'] != None and len(requestResults['preparingDownloads']) > 0:
                payload = {'label': label}


            #     print("hhhh")
            #     # print(len(moreDownloadUrls))
            #     for download in moreDownloadUrls['available']:
            #         if str(download['downloadId']) in requestResults['newRecords'] or str(download['downloadId']) in requestResults['duplicateProducts']:
            #             downloadIds.append(download['downloadId'])
            #             # print(len(downloadIds))
            #             print("DOWNLOAD: " + download['url'])

            #     # for download in moreDownloadUrls['requested']:
            #     #     if str(download['downloadId']) in requestResults['newRecords'] or str(download['downloadId']) in requestResults['duplicateProducts']:
            #     #         downloadIds.append(download['downloadId'])
            #     #         print("DOWNLOAD: " + download['url'])

            #     # # Didn't get all of the reuested downloads, call the download-retrieve method again probably after 30 seconds
            #     # while len(downloadIds) < (requestedDownloadsCount - len(requestResults['failed'])):
            #     #     preparingDownloads = requestedDownloadsCount - len(downloadIds) - len(requestResults['failed'])
            #     #     print("\n", preparingDownloads, "downloads are not available. Waiting for 30 seconds.\n")
            #     #     time.sleep(30)
            #     #     print("Trying to retrieve data\n")
            #     #     moreDownloadUrls = send_request(serviceUrl + "download-retrieve", payload, newApiKey)
            #     #     for download in moreDownloadUrls['available']:
            #     #         if download['downloadId'] not in downloadIds and (str(download['downloadId']) in requestResults['newRecords'] or str(download['downloadId']) in requestResults['duplicateProducts']):
            #     #             downloadIds.append(download['downloadId'])
            #     #             print("DOWNLOAD: " + download['url'])

            # else:
                # Get all available downloads
            urls = []
            # print(len(requestResults['availableDownloads']))
                # while len(requestResults['availableDownloads']) < print(len(scenes['results'])):
            for download in requestResults['availableDownloads']:
                    # print("DOWNLOAD: " + download['url'])
                    urls.append(download['url'])
            print("\nAll downloads are available to download.\n")
            lisr = []
            if sat == "ASTER Level 1T V3" or sat == "viirs_vnp13a1" or sat == "modis_mod13q1_v61" or sat == "modis_mod11a2_v61":
                for i in scenes['results']:
                    ob = {}
                    ob['ID'] = i['displayId']
                    ob['date'] = i['publishDate']
                    ob['path'] = i['displayId'][10:13]
                    ob['row'] = i['displayId'][13:16]
                    try:
                        ob['img'] = i['browse'][0]['browsePath']
                    except:
                         ob['img'] = "None"
                         continue
                         
                    print(i['browse'])
                    ob['foot'] = i['spatialBounds']['coordinates'][0]
                    lisr.append(ob)
            elif sat == LANDSAT_8_9 or sat == LANDSAT_4_5 or sat == "viirs_vnp21" or sat == "viirs_vnp13a2" or sat == "viirs_vnp13c2" or sat == "viirs_vnp13c1":
                            for i in scenes['results']:
                                ob = {}
                                ob['ID'] = i['displayId']
                                ob['date'] = i['publishDate']
                                ob['path'] = i['displayId'][10:13]
                                ob['row'] = i['displayId'][13:16]
                                try:
                                    ob['img'] = i['browse'][0]['browsePath']
                                except:
                                    ob['img'] = "None"
                                     
                                print(i['browse'])
                                try:
                                    ob['foot'] = i['spatialCoverage']['coordinates'][0]
                                except:
                                    ob['foot'] = "None"
                                     
                                lisr.append(ob)
            elif sat == "viirs_vnp13a3":
                            for i in scenes['results']:
                                ob = {}
                                ob['ID'] = i['displayId']
                                ob['date'] = i['publishDate']
                                ob['path'] = i['displayId'][10:13]
                                ob['row'] = i['displayId'][13:16]
                 
                                ob['foot'] = i['spatialCoverage']['coordinates'][0]
                                lisr.append(ob)
            elif sat == "emodis_global_lst_v6":
                            for i in scenes['results']:
                                ob = {}
                                try:
                                    ob['ID'] = i['displayId']
                                except:
                                    ob['ID'] = "None"
                                     
                                try:
                                    ob['date'] = i['publishDate']
                                except:
                                    ob['date'] = "None"
                                     
                                try:
                                    ob['path'] = i['displayId'][10:13]
                                except:
                                    ob['path'] = "None"
                                     
                                try:
                                    ob['row'] = i['displayId'][13:16]
                                except:
                                    ob['row'] = "None"
                                     
                
                                try:
                                  print(i['browse'][-1]['browsePath'])
                                except:
                                    print("None")
                                     
                                try:
                                    ob['foot'] = i['spatialCoverage']['coordinates'][0]
                                except:
                                    ob['foot'] = 'None'
                                     
                                lisr.append(ob)
            else:
                            print("Error")                

            urls.reverse()
            # print(lisr)
            c = 0
            for i in lisr:
                i['url'] = urls[c]
                c += 1

        else:
            print("Search found no results.\n")

    try: 
        return jsonify({'geodata': lisr})
    except:
        return jsonify({'geodata': []})
    # return 'file uploaded successfully'
   


@app.route('/download_all', methods=['POST'])
def download_all():
    data = request.get_json()
    urls = data["urls"]
    sat = data["sat"]

    if sat in sats:
        username = 'marwaneessalhi12'
        password = 'Morroco@0000'

        # Define a function to download a single URL with progress bar
        def download_url(url):
            with requests.Session() as session:
                session.auth = (username, password)
                r1 = session.request('get', url, stream=True)
                r = session.get(r1.url, auth=(username, password), stream=True)
                if r.ok:
                    print("Aster")  
                    if sat == "ASTER Level 1T V3":
                        file_name = url[20:]
                        total_size = int(r.headers.get('Content-Length', 0))
                        progress_bar = tqdm(total=total_size, unit='B', unit_scale=True)
                        with open(file_name+".hdf", "wb") as file:
                            for chunk in r.iter_content(chunk_size=8192):
                                if chunk:
                                    file.write(chunk)
                                    progress_bar.update(len(chunk))
                        progress_bar.close()
                    else:
                        file_name = url[63:]
                    
                        total_size = int(r.headers.get('Content-Length', 0))
                        progress_bar = tqdm(total=total_size, unit='B', unit_scale=True)
                        with open(file_name, "wb") as file:
                            for chunk in r.iter_content(chunk_size=8192):
                                if chunk:
                                    file.write(chunk)
                                    progress_bar.update(len(chunk))
                        progress_bar.close()
                        print("File downloaded successfully: ", file_name)
                # Use concurrent.futures.ThreadPoolExecutor to download URLs in parallel
        with concurrent.futures.ThreadPoolExecutor() as executor:
            # Submit download_url function for each URL
            executor.map(download_url, urls)
    else:
        print('here as ')
        def download_url(url):
            r = requests.get(url, stream=True)
            print(r.status_code)
            if r.ok:
                if sat == "Landsat 8-9 OLI/TIRS C2 L2":
                    file_name = url[url.index("LC"):url.index("T1")]
                elif sat == "Landsat 4-5 TM C2 L2":
                    file_name = url[url.index("LT"):url.index("T1")]
                else:
                    file_name = url[url.index("AST"):]

                total_size = int(r.headers.get('Content-Length', 0))
                progress_bar = tqdm(total=total_size, unit='B', unit_scale=True, desc=file_name)




                with open(file_name+".tar", "wb") as file:
                        for chunk in r.iter_content(chunk_size=8192):
                            if chunk:
                                file.write(chunk)
                                progress_bar.update(len(chunk))
                progress_bar.close()
            else:
                print("Not ok!!!")
                # Use concurrent.futures.ThreadPoolExecutor to download URLs in parallel
        with concurrent.futures.ThreadPoolExecutor() as executor:
            # Submit download_url function for each URL
            futures = [executor.submit(download_url, url) for url in urls]

            # Wait for all downloads to complete.
            concurrent.futures.wait(futures)




    return "Done"

# Defining the route of coordinates manual input. 
@app.route('/addcoords', methods=['POST'])
def points():

    data = request.get_json()
    points = data["polyp"]
    print(points)
    sat = data["sat"]

    sat = data["sat"]
    dateFrom = data["dateFrom"] + ' 00:00:00'
    dateTo = data["dateTo"] + ' 00:00:00'
    clMax = data["cloudMax"]
    clMin = data["cloudMin"]
    print(dateFrom)
    print(dateTo)

    print("\nRunning Scripts...\n")

    serviceUrl = "https://m2m.cr.usgs.gov/api/api/json/development/"

    # login
    payload = {'username': 'marouan.essalhi@uit.ac.ma',
               'password': "Morroco@1212"}

    newApiKey = send_request(serviceUrl + "login", payload)
    temporalFilter = {'start': dateFrom, 'end': dateTo}
    CloudCoverFilter = {"min": int(clMin), "max": int(
        clMax), "includeUnknown": True}

    print("API Key: " + newApiKey + "\n")


    if sat in datasetname_mapping:
        datasetname = datasetname_mapping[sat]

    print("#"*100)
    print(points)
    n_points = []
    for i in points:
        n_points.append([i[1], i[0]])
    print(n_points)

    payload = {
        "datasetName": datasetname,
        # "temporalFilter" : temporalFilter,
        # "cloudCoverFilter": CloudCoverFilter,
        "maxResults": "50",
        "metadataType": "summary",
        "sceneFilter": {
            "acquisitionFilter": temporalFilter,
            "cloudCoverFilter": CloudCoverFilter,
            "spatialFilter": {
                        "geoJson": {
                            "type": "Polygon",
                            "coordinates": [
                                n_points
                            ]
                        },
                "filterType": "geojson"
            }

        }
    }

    print("Searching datasets...\n")
    scenes = send_request(serviceUrl + "scene-search", payload, newApiKey)



    print(len(scenes['results']))
    if scenes['recordsReturned'] > 0:
        # Aggregate a list of scene ids
        sceneIds = []
        for result in scenes['results']:
            # Add this scene to the list I would like to download
            sceneIds.append(result['entityId'])
            # print(result['entityId'])
        # Find the download options for these scenes
        # NOTE :: Remember the scene list cannot exceed 50,000 items!
        payload = {'datasetName': datasetname.lower(), 'entityIds': sceneIds}

        downloadOptions = send_request(
            serviceUrl + "download-options", payload, newApiKey)
        # print(downloadOptions)

        # Aggregate a list of available products
        downloads = []
        print(downloadOptions)
        # try:
        for product in downloadOptions:
                # Make sure the product is available for this scene
                if product['available'] == True:
                    downloads.append({'entityId': product['entityId'],
                                    'productId': product['id']})
                    print("done!!!")
                else:
                    print("No products are available")
        # except:
        #     print("Failed to find products")
            #  

        print("we are here!!!!!!!")
        if downloads:

            label = datetime.datetime.now().strftime(
                "%Y%m%d_%H%M%S")  # Customized label using date time
            payload = {'downloads': downloads,
                       'label': label}
            # Call the download to get the direct download urls
            requestResults = send_request(
                serviceUrl + "download-request", payload, newApiKey)

            # PreparingDownloads has a valid link that can be used but data may not be immediately available
            # Call the download-retrieve method to get download that is available for immediate download
            if requestResults['preparingDownloads'] != None and len(requestResults['preparingDownloads']) > 0:
                payload = {'label': label}


            #     print("hhhh")
            #     # print(len(moreDownloadUrls))
            #     for download in moreDownloadUrls['available']:
            #         if str(download['downloadId']) in requestResults['newRecords'] or str(download['downloadId']) in requestResults['duplicateProducts']:
            #             downloadIds.append(download['downloadId'])
            #             # print(len(downloadIds))
            #             print("DOWNLOAD: " + download['url'])

            #     # for download in moreDownloadUrls['requested']:
            #     #     if str(download['downloadId']) in requestResults['newRecords'] or str(download['downloadId']) in requestResults['duplicateProducts']:
            #     #         downloadIds.append(download['downloadId'])
            #     #         print("DOWNLOAD: " + download['url'])

            #     # # Didn't get all of the reuested downloads, call the download-retrieve method again probably after 30 seconds
            #     # while len(downloadIds) < (requestedDownloadsCount - len(requestResults['failed'])):
            #     #     preparingDownloads = requestedDownloadsCount - len(downloadIds) - len(requestResults['failed'])
            #     #     print("\n", preparingDownloads, "downloads are not available. Waiting for 30 seconds.\n")
            #     #     time.sleep(30)
            #     #     print("Trying to retrieve data\n")
            #     #     moreDownloadUrls = send_request(serviceUrl + "download-retrieve", payload, newApiKey)
            #     #     for download in moreDownloadUrls['available']:
            #     #         if download['downloadId'] not in downloadIds and (str(download['downloadId']) in requestResults['newRecords'] or str(download['downloadId']) in requestResults['duplicateProducts']):
            #     #             downloadIds.append(download['downloadId'])
            #     #             print("DOWNLOAD: " + download['url'])

            # else:
                # Get all available downloads
            urls = []
            # print(len(requestResults['availableDownloads']))
                # while len(requestResults['availableDownloads']) < print(len(scenes['results'])):
            for download in requestResults['availableDownloads']:
                    print("DOWNLOAD: " + download['url'])
                    urls.append(download['url'])
            print("\nAll downloads are available to download.\n")
            lisr = []
            if sat == "ASTER Level 1T V3" or sat == "viirs_vnp13a1" or sat == "modis_mod13q1_v61" or sat == "modis_mod11a2_v61":
                for i in scenes['results']:
                    ob = {}
                    ob['ID'] = i['displayId']
                    ob['date'] = i['publishDate']
                    ob['path'] = i['displayId'][10:13]
                    ob['row'] = i['displayId'][13:16]
                    try:
                        ob['img'] = i['browse'][0]['browsePath']
                    except:
                         ob['img'] = "None"
                         continue
                         
                    print(i['browse'])
                    ob['foot'] = i['spatialBounds']['coordinates'][0]
                    lisr.append(ob)
            elif sat == LANDSAT_8_9 or sat == LANDSAT_4_5 or sat == "viirs_vnp21" or sat == "viirs_vnp13a2" or sat == "viirs_vnp13c2" or sat == "viirs_vnp13c1":
                            for i in scenes['results']:
                                ob = {}
                                ob['ID'] = i['displayId']
                                ob['date'] = i['publishDate']
                                ob['path'] = i['displayId'][10:13]
                                ob['row'] = i['displayId'][13:16]
                                try:
                                    ob['img'] = i['browse'][0]['browsePath']
                                except:
                                    ob['img'] = "None"
                                     
                                print(i['browse'])
                                try:
                                    ob['foot'] = i['spatialCoverage']['coordinates'][0]
                                except:
                                    ob['foot'] = "None"
                                     
                                lisr.append(ob)
            elif sat == "viirs_vnp13a3":
                            for i in scenes['results']:
                                ob = {}
                                ob['ID'] = i['displayId']
                                ob['date'] = i['publishDate']
                                ob['path'] = i['displayId'][10:13]
                                ob['row'] = i['displayId'][13:16]
                 
                                ob['foot'] = i['spatialCoverage']['coordinates'][0]
                                lisr.append(ob)
            elif sat == "emodis_global_lst_v6":
                            for i in scenes['results']:
                                ob = {}
                                try:
                                    ob['ID'] = i['displayId']
                                except:
                                    ob['ID'] = "None"
                                     
                                try:
                                    ob['date'] = i['publishDate']
                                except:
                                    ob['date'] = "None"
                                     
                                try:
                                    ob['path'] = i['displayId'][10:13]
                                except:
                                    ob['path'] = "None"
                                     
                                try:
                                    ob['row'] = i['displayId'][13:16]
                                except:
                                    ob['row'] = "None"
                                     
                
                                try:
                                  print(i['browse'][-1]['browsePath'])
                                except:
                                    print("None")
                                     
                                try:
                                    ob['foot'] = i['spatialCoverage']['coordinates'][0]
                                except:
                                    ob['foot'] = 'None'
                                     
                                lisr.append(ob)
            else:
                            print("Error")                

            urls.reverse()
            # print(lisr)
            c = 0
            for i in lisr:
                i['url'] = urls[c]
                c += 1

        else:
            print("Search found no results.\n")

    try: 
        return jsonify({'geodata': lisr})
    except:
        return jsonify({'geodata': []})
         
# if __name__ == '__main__':

@app.route('/shapefile', methods=["POST"])
def upload_shape():
    file = request.files['myfile']
    print(file)
    # Read the shapefile
    data = gpd.read_file(file)
    print(data)

    # Extract the coordinates
    coordinates = []
    for geom in data.geometry:
        #If the geometry is for a polygon.
        if geom.geom_type == 'Polygon':
            coordinates.append(geom.exterior.coords[:])
        #Geometry of a Multipolygon.
        elif geom.geom_type == 'MultiPolygon':
            for polygon in geom.geoms:
                coordinates.append(polygon.exterior.coords[:])
        else:
            # Handle other geometry types if needed
            pass

    # Print the coordinates
    for coords in coordinates:
        print(coords)
        print("#"*500)
        return

# Defining the route to map use.
@app.route('/usemap', methods=['POST'])
def usemap():
    data = request.get_json()
    # print(data)
    coordinates = data['coordinates']
    sat = data['sat']
    lst = list(coordinates[0])
    tuple_lst = set(map(tuple, lst))
    lst = map(list, tuple_lst)
    seen = set()
    lst = [item for item in lst if not (
        tuple(item) in seen or seen.add(tuple(item)))]
    lst.append(lst[0])
    print(lst)
    dateFrom = data["dateFrom"] + ' 00:00:00'
    dateTo = data["dateTo"] + ' 00:00:00'
    clMax = data["cloudMa"]
    clMin = data["cloudMin"]


    print("\nRunning Scripts...\n")

    serviceUrl = "https://m2m.cr.usgs.gov/api/api/json/development/"

    # login
    payload = {'username': 'marouan.essalhi@uit.ac.ma',
               'password': "Morroco@1212"}

    newApiKey = send_request(serviceUrl + "login", payload)
    temporalFilter = {'start': dateFrom, 'end': dateTo}
    CloudCoverFilter = {"min": int(clMin), "max": int(
        clMax), "includeUnknown": True}

    print("API Key: " + newApiKey + "\n")

    if sat == LANDSAT_8_9:
        datasetname = "landsat_ot_c2_l2"
    elif sat == LANDSAT_4_5:
        datasetname = "landsat_tm_c2_l2"
    elif sat == "ASTER Level 1T V3":
        datasetname = "aster_l1t"
    elif sat == "viirs_vnp13a1":
        datasetname = "viirs_vnp13a1"
    elif sat == "viirs_vnp21":
        datasetname = "viirs_vnp21"
    elif sat == "viirs_vnp13a2":
        datasetname = "viirs_vnp13a2"
    elif sat == "viirs_vnp13c2":
        datasetname = "viirs_vnp13c2"
    elif sat == "viirs_vnp13c1":
        datasetname = "viirs_vnp13c1"
    elif sat == "viirs_vnp13a3":
        datasetname = "viirs_vnp13a3"
    elif sat == "emodis_global_lst_v6":
        datasetname = "emodis_global_lst_v6"
    elif sat == "modis_mod13q1_v61":
        datasetname = "modis_mod13q1_v61"
    elif sat == "modis_mod11a2_v61":
        datasetname = "modis_mod11a2_v61"



    payload = {
        "datasetName": datasetname,
        # "temporalFilter" : temporalFilter,
        # "cloudCoverFilter": CloudCoverFilter,
        "maxResults": "5000",
        "metadataType": "summary",
        "sceneFilter": {
            "acquisitionFilter": temporalFilter,
            "cloudCoverFilter": CloudCoverFilter,
            "spatialFilter": {
                        "geoJson": {
                            "type": "Polygon",
                            "coordinates": [
                                lst
                            ]
                        },
                "filterType": "geojson"
            }

        }
    }

    print("Searching datasets...\n")
    scenes = send_request(serviceUrl + "scene-search", payload, newApiKey)

    print(scenes)
    print("#"*100)


    if scenes['recordsReturned'] > 0:
        # Aggregate a list of scene ids
        sceneIds = []
        for result in scenes['results']:
            # Add this scene to the list I would like to download
            sceneIds.append(result['entityId'])
            # print(result['entityId'])
        # Find the download options for these scenes
        # NOTE :: Remember the scene list cannot exceed 50,000 items!
        payload = {'datasetName': datasetname.lower(), 'entityIds': sceneIds}

        downloadOptions = send_request(
            serviceUrl + "download-options", payload, newApiKey)
        # print(downloadOptions)

        # Aggregate a list of available products
        downloads = []
        for product in downloadOptions:
            # Make sure the product is available for this scene
            if product['available'] == True:
                downloads.append({'entityId': product['entityId'],
                                  'productId': product['id']})


        # Did we find products?
        if downloads:

            # set a label for the download request
            label = datetime.datetime.now().strftime(
                "%Y%m%d_%H%M%S")  # Customized label using date time
            payload = {'downloads': downloads,
                       'label': label}
            # Call the download to get the direct download urls
            requestResults = send_request(
                serviceUrl + "download-request", payload, newApiKey)

            # PreparingDownloads has a valid link that can be used but data may not be immediately available
            # Call the download-retrieve method to get download that is available for immediate download
            if requestResults['preparingDownloads'] != None and len(requestResults['preparingDownloads']) > 0:
                payload = {'label': label}

 
                # # Didn't get all of the reuested downloads, call the download-retrieve method again probably after 30 seconds
                # while len(downloadIds) < (requestedDownloadsCount - len(requestResults['failed'])):
                #     preparingDownloads = requestedDownloadsCount - len(downloadIds) - len(requestResults['failed'])
                #     print("\n", preparingDownloads, "downloads are not available. Waiting for 30 seconds.\n")
                #     time.sleep(30)
                #     print("Trying to retrieve data\n")
                #     moreDownloadUrls = send_request(serviceUrl + "download-retrieve", payload, newApiKey)
                #     for download in moreDownloadUrls['available']:
                #         if download['downloadId'] not in downloadIds and (str(download['downloadId']) in requestResults['newRecords'] or str(download['downloadId']) in requestResults['duplicateProducts']):
                #             downloadIds.append(download['downloadId'])
                #             print("DOWNLOAD: " + download['url'])

            # else:
                # Get all available downloads
            urls = []
            print(len(requestResults['availableDownloads']))
                # while len(requestResults['availableDownloads']) < print(len(scenes['results'])):
            for download in requestResults['availableDownloads']:
    
                    urls.append(download['url'])
            print("\nAll downloads are available to download.\n")
            lisr = []
            if sat == "ASTER Level 1T V3" or sat == "viirs_vnp13a1" or sat == "modis_mod13q1_v61" or sat == "modis_mod11a2_v61":
                for i in scenes['results']:
                    ob = {}
                    ob['ID'] = i['displayId']
                    ob['date'] = i['publishDate']
                    ob['path'] = i['displayId'][10:13]
                    ob['row'] = i['displayId'][13:16]
                    ob['img'] = i['browse'][0]['browsePath']
                    print(i['browse'])
                    ob['foot'] = i['spatialBounds']['coordinates'][0]
                    lisr.append(ob)
            elif sat == LANDSAT_8_9 or sat == LANDSAT_4_5 or sat == "viirs_vnp21" or sat == "viirs_vnp13a2" or sat == "viirs_vnp13c2" or sat == "viirs_vnp13c1":
                for i in scenes['results']:
                    ob = {}
                    ob['ID'] = i['displayId']
                    ob['date'] = i['publishDate']
                    ob['path'] = i['displayId'][10:13]
                    ob['row'] = i['displayId'][13:16]
                    try:
                        ob['img'] = i['browse'][0]['browsePath']
                    except:
                        ob['img'] = "None"
                         
                    print(i['browse'])
                    try:
                        ob['foot'] = i['spatialCoverage']['coordinates'][0]
                    except:
                        ob['foot'] = "None"
                         

                    lisr.append(ob)
            elif sat == "viirs_vnp13a3":
                for i in scenes['results']:
                    ob = {}
                    ob['ID'] = i['displayId']
                    ob['date'] = i['publishDate']
                    ob['path'] = i['displayId'][10:13]
                    ob['row'] = i['displayId'][13:16]
                    try:
                        print(i['browse'][-1]['browsePath'])
                    except:
                        print("None")
                         
                    ob['foot'] = i['spatialCoverage']['coordinates'][0]
                    lisr.append(ob)
            elif sat == "emodis_global_lst_v6":
                for i in scenes['results']:
                    ob = {}
                    try:
                        ob['ID'] = i['displayId']
                    except:
                        ob['ID'] = "None"
                         
                    try:
                         ob['date'] = i['publishDate']
                    except:
                        
                        ob['date'] = "None"
                         
                    try:
                        ob['path'] = i['displayId'][10:13]
                    except:
                        ob['path'] = "None"
                         
                    try:
                        ob['row'] = i['displayId'][13:16]
                    except:
                        ob['row'] = "None"
                         
                    try:
                     print(i['browse'][-1]['browsePath'])
                    except:
                        print("None")
                         
                    try:
                        ob['foot'] = i['spatialCoverage']['coordinates'][0]
                    except:
                        
                        ob['foot'] = 'None'
                         
                    lisr.append(ob)
            else:
                print("Error")
                # print("Error")
            urls.reverse()
            # print(lisr)
            c = 0
            for i in lisr:
                i['url'] = urls[c]
                c += 1


    else:
        print("Search found no results.\n")
    # print(lisr)
    # Logout so the API Key cannot be used anymore
    endpoint = "logout"
    if send_request(serviceUrl + endpoint, None, newApiKey) == None:
        print("Logged Out\n\n")
    else:
        print("Logout Failed\n\n")
    try:
        return jsonify({'geodata': lisr})
    except:
        return jsonify({'geodata': []})
         

if __name__ == '__main__':
    app.run()
# if __name__ == '__main__':
#     webbrowser.open('http://127.0.0.1:5000')