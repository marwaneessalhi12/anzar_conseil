from sentinelhub import DataCollection, BBox, CRS, DownloadRequest, SHConfig

# Set your Sentinel Hub credentials
CLIENT_ID = 'your-client-id'
CLIENT_SECRET = 'your-client-secret'

# Configure the Sentinel Hub API
config = SHConfig()
config.sh_client_id = CLIENT_ID
config.sh_client_secret = CLIENT_SECRET

# Define the bounding box and resolution
bbox = BBox((13.40, 52.50, 13.45, 52.55), CRS.WGS84)
resolution = 10  # in meters

# Define the time range
time_interval = ('2022-01-01', '2022-01-10')

# Define the image format
image_format = 'tif'

# Create a download request
request = DownloadRequest(
    data_collection=DataCollection.SENTINEL2_L1C,
    bbox=bbox,
    time=time_interval,
    resolution=resolution,
    image_format=image_format,
    config=config
)

# Perform the download
download_path = request.download(save_to='path/to/save/folder')

# Print the path to the downloaded file
print('Downloaded file:', download_path)